# SnowTricks

Snowtricks project, OpenClassrooms Backend/Symfony Developer path, project #6

  - clone into docker container folder (https://gitlab.com/DamienAdam/dockersnowtricks.git)
  - run `sh runner php dev` from container folder
  - run `php bin/console composer install` to install vendors
  - go to 127.0.0.1

nb: phpmyadmin available at 127.0.0.1:8000 for your convenience
