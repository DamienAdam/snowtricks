<?php

namespace DataFixtures;

use App\Domain\Trick\Doctrine\Entity\Grab;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class GrabFixtures extends Fixture
{
    public function load(ObjectManager $objectManager)
    {
        $RAW_QUERY = 'ALTER TABLE grab AUTO_INCREMENT = 1';
        $conn = $objectManager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        $grabs = ['A B','Beef Carpaccio','Beef Curtains','Bloody Dracula','Canadian Bacon','One-Two'];

        foreach ($grabs as $grabName) {
            $grab = new Grab();
            $grab->setName($grabName);

            $objectManager->persist($grab);
        }

        $objectManager->flush();

    }
}