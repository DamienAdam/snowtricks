<?php

namespace DataFixtures;

use App\Common\Utils\TextUtils;
use App\Domain\User\Doctrine\Entity\User;
use App\Domain\User\Manager\UserManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param ObjectManager $objectManager
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function load(ObjectManager $objectManager)
    {
        $RAW_QUERY = 'ALTER TABLE user AUTO_INCREMENT = 1';
        $conn = $objectManager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        for ($i = 0; $i < 10; $i++) {

            $user = new User();
            $user = $user
                ->setEmail('user'.$i.'@gmail.com')
                ->setName('user'.$i)
                ->setPlainPassword('pwd'.$i)
                ->setValidationToken(TextUtils::generateRandomString(UserManager::VALIDATION_TOKEN_LENGTH))
                ->setValidatedAt(new \DateTime())
                ;

            $this->userManager->createUser($user, false);
        }
    }
}