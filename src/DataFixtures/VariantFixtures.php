<?php

namespace DataFixtures;

use App\Domain\Trick\Doctrine\Entity\Variant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class VariantFixtures extends Fixture
{
    public function load(ObjectManager $objectManager)
    {
        $RAW_QUERY = 'ALTER TABLE variant AUTO_INCREMENT = 1';
        $conn = $objectManager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        $variants = ['Butter','Jib','Manual','One-footed','Shifty','Stiffy','Stink-bug','Tuck knee'];

        foreach ($variants as $variantName) {
            $variant = new Variant();
            $variant->setName($variantName);

            $objectManager->persist($variant);
        }

        $objectManager->flush();
    }
}