<?php

namespace DataFixtures;

use App\Domain\Trick\Doctrine\Entity\TrickVideo;
use App\Domain\Trick\Doctrine\Repository\TrickRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TrickVideoFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @var TrickRepository
     */
    private $trickRepository;

    public function __construct(TrickRepository $trickRepository)
    {
        $this->trickRepository = $trickRepository;
    }

    public function load(ObjectManager $objectManager)
    {
        $RAW_QUERY = 'ALTER TABLE trick_video AUTO_INCREMENT = 1';
        $conn = $objectManager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        $videoUrls = [
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/t705_V-RDcQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/1TJ08caetkw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/FuZc3fTmUnc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/ny-51KAp0eM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/AtDbPEGr7Tc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/EN_GE9sGLL0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/AzJPhQdTRQQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/1AibZIwxnuU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/MnRNHpj_jwY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/axNnKy-jfWw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
        ];

//        $videoUrls = [
//            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'
//        ];

        for ($i = 0; $i < 20; $i++) {
            $urlId = rand(0, 9);
            $trickVideo = $this->createTrickVideo($videoUrls[$urlId], rand(1, 16));
            $objectManager->persist($trickVideo);
        }

        $objectManager->flush();
    }

    public function getDependencies()
    {
        return [
            TrickFixtures::class
        ];
    }

    /**
     * @param $url
     * @param $trickId
     * @return TrickVideo
     */
    private function createTrickVideo($url, $trickId)
    {
        $trickVideo = new TrickVideo();
        $trickVideo->setUrl($url);
        $trickVideo->setTrick($this->trickRepository->findOneBy(['id' => $trickId]));

        return $trickVideo;
    }
}