<?php

namespace App\Infrastructure\Mailer\Mailjet\Manager;

use App\Domain\User\Doctrine\Entity\User;
use App\Infrastructure\Mailer\Mailjet\Form\Command\MailjetCommand;
use App\Infrastructure\Mailer\Mailjet\Form\Command\MailjetContactCommand;
use App\Infrastructure\Mailer\Mailjet\Form\Command\MailjetVariableCommand;
use Mailjet\Client;
use Mailjet\Resources;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MailjetManager
{
    /**
     * @var Client
     */
    private $mailjet;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * MailjetManager constructor.
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->mailjet = new Client('3fc1b2dc2e10be967c90730e45cff19e', '6a7ce667856fcbec098ee45f7202659f');
        $this->urlGenerator = $urlGenerator;
    }

    public function sendEmail(MailjetCommand $mailjetCommand)
    {
        return $this->mailjet->post(Resources::$Email, [
            'body' => $mailjetCommand->getBody()
        ]);
    }

    /**
     * @param User $user
     * @return MailjetCommand
     */
    public function createNewForResetPassword(User $user)
    {
        $mailjetCommand = new MailjetCommand();

        $from = new MailjetContactCommand();
        $from->email = 'damien.adam@heureetcontrole.fr';
        $from->name = 'Snowtricks housekeeping';
        $mailjetCommand->from = $from;

        $dest = new MailjetContactCommand();
        $dest->email = $user->getEmail();
        $dest->name = $user->getName();
        $mailjetCommand->dest = $dest;

        $mailjetCommand->subject = 'Reset password request';

        $var1 = new MailjetVariableCommand();
        $var1->key = 'resetLink';
        $var1->value = $this->urlGenerator->generate('security_password_reset_form', [
            'resetToken' => $user->getResetToken()
        ],UrlGeneratorInterface::ABSOLUTE_URL);

        $mailjetCommand->vars[] = $var1;

        $mailjetCommand->templateId = 989255;

        return $mailjetCommand;
    }

}