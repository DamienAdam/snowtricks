<?php

namespace App\Infrastructure\Mailer\Mailjet\Form\Command;

class MailjetAttachmentCommand
{
    public $contentType;

    public $fileName;

    public $content;
}