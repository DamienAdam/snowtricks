<?php

namespace App\Common\Doctrine\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class BaseRepository extends EntityRepository
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Mapping\ClassMetadata
     */
    private $class;

    public function __construct(EntityManagerInterface $entityManager, Mapping\ClassMetadata $class)
    {
        parent::__construct($entityManager, $class);

        $this->entityManager = $entityManager;
        $this->class = $class;

        $this->entityManager->getConfiguration()->setSQLLogger(null);
    }

    /**
     * @param $entity
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param $entity
     * @throws ORMException
     */
    public function removeAndFlush($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();

    }

    /**
     * @param $entity
     */
    public function persist($entity)
    {
        $this->persist($entity);
    }

    public function flush()
    {
        $this->flush();
    }
}