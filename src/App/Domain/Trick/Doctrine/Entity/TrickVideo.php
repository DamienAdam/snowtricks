<?php

namespace App\Domain\Trick\Doctrine\Entity;

class TrickVideo
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var Trick
     */
    private $trick;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return TrickVideo
     */
    public function setUrl(string $url): TrickVideo
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @param string $url
     * @return TrickVideo
     */
    public function setFileName(string $url): TrickVideo
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @param Trick|null $trick
     *
     * @return TrickVideo
     */
    public function setTrick(Trick $trick = null)
    {
        $this->trick = $trick;

        return $this;
    }

    /**
     * @return Trick|null
     */
    public function getTrick()
    {
        return $this->trick;
    }
}