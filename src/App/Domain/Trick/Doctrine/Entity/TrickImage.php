<?php

namespace App\Domain\Trick\Doctrine\Entity;

class TrickImage
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var Trick
     */
    private $trick;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return TrickImage
     */
    public function setFileName(string $fileName): TrickImage
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @param Trick|null $trick
     *
     * @return TrickImage
     */
    public function setTrick(Trick $trick = null)
    {
        $this->trick = $trick;

        return $this;
    }

    /**
     * @return Trick|null
     */
    public function getTrick()
    {
        return $this->trick;
    }
}