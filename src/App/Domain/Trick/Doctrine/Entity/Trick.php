<?php

namespace App\Domain\Trick\Doctrine\Entity;

use App\Domain\Comment\Doctrine\Entity\Comment;
use App\Domain\User\Doctrine\Entity\User;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Exception;

class Trick
{
    const TRICK_DIRECTIONS = [
        'none',
        'frontside',
        'backside',
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $isSwitch;

    /**
     * @var int
     */
    private $rotation;

    /**
     * @var string
     */
    private $direction;

    /**
     * @var DateTime $created
     *
     */
    protected $createdAt;

    /**
     * @var DateTime $updated
     *
     */
    protected $updatedAt;

    /**
     * @var Collection
     */
    private $trickImages;

    /**
     * @var Collection
     */
    private $trickVideos;

    /**
     * @var Grab
     */
    private $grab;

    /**
     * @var Slide
     */
    private $slide;

    /**
     * @var Variant
     */
    private $variant;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var Collection
     */
    private $comments;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Trick
     */
    public function setName(string $name): Trick
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Trick
     */
    public function setDescription(string $description): Trick
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSwitch(): ?bool
    {
        return $this->isSwitch;
    }

    /**
     * @param bool $isSwitch
     * @return Trick
     */
    public function setIsSwitch(bool $isSwitch): Trick
    {
        $this->isSwitch = $isSwitch;

        return $this;
    }

    /**
     * @return int
     */
    public function getRotation(): ?int
    {
        return $this->rotation;
    }

    /**
     * @param int $rotation
     * @return Trick
     */
    public function setRotation(int $rotation): Trick
    {
        $this->rotation = $rotation;

        return $this;
    }

    /**
     * @return string
     */
    public function getDirection(): ?string
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     * @return Trick
     */
    public function setDirection(string $direction): Trick
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Run by doctrine lifecycle callbacks on pre-persist and pre-update
     *
     * @throws Exception
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new DateTime();

        $this->setUpdatedAt($dateTimeNow);

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    public function getCreatedAt() :?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt() :?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @param TrickImage $trickImage
     * @return Trick
     */
    public function addTrickImage(TrickImage $trickImage) : Trick
    {
        $this->trickImages[] = $trickImage;

        $trickImage->setTrick($this);

        return $this;
    }

    /**
     * @param TrickImage $trickImage
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrickImage(TrickImage $trickImage)
    {
        return $this->trickImages->removeElement($trickImage);
    }

    /**
     * @return Collection
     */
    public function getTrickImages()
    {
        return $this->trickImages;
    }

    /**
     * @param TrickVideo $trickVideo
     * @return Trick
     */
    public function addTrickVideo(TrickVideo $trickVideo) : Trick
    {
        $this->trickVideos[] = $trickVideo;

        $trickVideo->setTrick($this);

        return $this;
    }

    /**
     * @param TrickVideo $trickVideo
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrickVideo(TrickVideo $trickVideo)
    {
        return $this->trickVideos->removeElement($trickVideo);
    }

    /**
     * @return Collection
     */
    public function getTrickVideos()
    {
        return $this->trickVideos;
    }

    /**
     * Set grab.
     *
     * @param Grab|null $grab
     *
     * @return Trick
     */
    public function setGrab(Grab $grab = null)
    {
        $this->grab = $grab;

        return $this;
    }

    /**
     * Get grab.
     *
     * @return Grab|null
     */
    public function getGrab()
    {
        return $this->grab;
    }

    /**
     * Set slide.
     *
     * @param Slide|null $slide
     *
     * @return Trick
     */
    public function setSlide(Slide $slide = null)
    {
        $this->slide = $slide;

        return $this;
    }

    /**
     * Get slide.
     *
     * @return Slide|null
     */
    public function getSlide()
    {
        return $this->slide;
    }

    /**
     * Set variant.
     *
     * @param Variant|null $variant
     *
     * @return Trick
     */
    public function setVariant(Variant $variant = null)
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * Get variant.
     *
     * @return Variant|null
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param User|null $user
     *
     * @return Trick
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $slug
     * @return Trick
     */
    public function setSlug(string $slug) : Trick
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug() : string
    {
        return $this->slug;
    }

    /**
     * @param Comment $comment
     * @return Trick
     */
    public function addComment(Comment $comment) : Trick
    {
        $this->comments[] = $comment;

        $comment->setTrick($this);

        return $this;
    }

    /**
     * @param Comment $comment
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeComment(Comment $comment)
    {
        return $this->comments->removeElement($comment);
    }

    /**
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
}