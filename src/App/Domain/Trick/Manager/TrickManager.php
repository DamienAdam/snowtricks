<?php

namespace App\Domain\Trick\Manager;

use App\Domain\Trick\Doctrine\Entity\Trick;
use App\Domain\Trick\Doctrine\Repository\TrickRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class TrickManager
{
    /**
     * @var TrickRepository
     */
    private $trickRepository;

    /**
     * @var int
     */
    private $commentsPerPage;

    /**
     * @var TrickImageManager
     */
    private $trickImageManager;

    public function __construct(TrickRepository $trickRepository, int $commentsPerPage, TrickImageManager $trickImageManager)
    {
        $this->trickRepository = $trickRepository;
        $this->commentsPerPage = $commentsPerPage;
        $this->trickImageManager = $trickImageManager;
    }

    /**
     * @param Trick $trick
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Trick $trick)
    {
        $this->trickRepository->save($trick);
    }

    /**
     * @return array
     */
    public function findAll()
    {

        return $this->trickRepository->findBy( [], ['updatedAt' => 'DESC']);
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countAll()
    {
        return $this->trickRepository->countAll();
    }

    public function findPaginated(int $page)
    {
        return $this->trickRepository->findPaginated($this->commentsPerPage, $page);
    }

    /**
     * @param Trick $trick
     * @throws ORMException
     */
    public function delete(Trick $trick)
    {
        foreach ($trick->getTrickImages() as $trickImage) {
            $this->trickImageManager->unlinkImage($trickImage->getFileName());
        }

        $this->trickRepository->removeAndFlush($trick);
    }

}