<?php

namespace App\Domain\User\Controller;

use App\Domain\User\Doctrine\Entity\User;
use App\Domain\User\Form\Command\PasswordResetCommand;
use App\Domain\User\Form\Type\PasswordResetFormType;
use App\Domain\User\Form\Type\PasswordResetRequestType;
use App\Domain\User\Form\Type\SignupType;
use App\Domain\User\Mail\AccountValidationMail;
use App\Domain\User\Manager\UserManager;
use App\Infrastructure\Mailer\Mailjet\Form\Command\MailjetCommand;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function signup(Request $request)
    {
        $form = $this->createForm(SignupType::class, $user = new User());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UserManager $userManager */
            $userManager = $this->get('app.user.manager');
            $userManager->signup($user);

            $mailer = $this->get('app.mailer.mailjet.manager');

            /** @var MailjetCommand $mailjetCommand */
            $email = new AccountValidationMail($user, $this->get('router'));
            $sendMailResponse = $mailer->sendEmail($email->getMailjetCommand());

            if ($sendMailResponse->success()) {
                $this->addFlash('success', 'Your account has been created, you will receive a validation link by email soon');

                return $this->redirectToRoute('security_login');
            }

            $this->addFlash('danger', 'Something went wrong, please try again or contact us');

            return $this->redirectToRoute('security_signup');
        }

        return $this->render('User/signup.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @param $validationToken
     * @return RedirectResponse|HttpException
     * @throws Exception
     */
    public function confirmSignup($validationToken)
    {
        /** @var UserManager $userManager */
        $userManager = $this->get('app.user.manager');
        $user = $userManager->findOneByValidationToken($validationToken);

        if(!$user) {
            $this->addFlash('error', 'Your validation link is invalid, please retry signup');

            throw new BadRequestHttpException('Invalid parameter: validation token');
        }

        $user->setValidatedAt(new DateTime());
        $userManager->save($user);
        $this->addFlash('success', 'Your email has been confirmed, you can now login');

        return $this->redirectToRoute('security_login');
    }

    /**
     * @return Response
     */
    public function login()
    {
        /** @var AuthenticationUtils $authenticationUtils */
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('User/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }
}
