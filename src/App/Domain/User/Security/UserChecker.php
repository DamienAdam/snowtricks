<?php

namespace App\Domain\User\Security;

use App\Domain\User\Doctrine\Entity\User;
use Exception;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{

    /**
     * Checks the user account before authentication.
     *
     * @param UserInterface $user
     * @return void
     * @throws Exception
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }
    }

    /**
     * Checks the user account after authentication.
     *
     * @param UserInterface $user
     * @return void
     */
    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }

        if (!$user->getValidatedAt()) {
            throw new CustomUserMessageAuthenticationException('Account invalid (did you receive a validation link by email ?)');
        }
    }
}