<?php

namespace App\Domain\User\Security;

use App\Domain\User\Doctrine\Entity\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices as TBRMS;

class TokenBasedRememberMeServices extends TBRMS
{
    protected function onLoginSuccess(Request $request, Response $response, TokenInterface $token)
    {
        /** @var User $user */
        $user = $token->getUser();
        $expires = time() + $this->options['lifetime'];
        $value = $this->generateCookieValue(\get_class($user), $user->getEmail(), $expires, $user->getPassword());

        $response->headers->setCookie(
            new Cookie(
                $this->options['name'],
                $value,
                $expires,
                $this->options['path'],
                $this->options['domain'],
                $this->options['secure'] ?? $request->isSecure(),
                $this->options['httponly'],
                false,
                $this->options['samesite']
            )
        );
    }
}