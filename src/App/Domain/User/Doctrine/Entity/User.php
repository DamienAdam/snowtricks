<?php

namespace App\Domain\User\Doctrine\Entity;

use App\Domain\Comment\Doctrine\Entity\Comment;
use App\Domain\Trick\Doctrine\Entity\Trick;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Exception;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, EquatableInterface
{
    const ROLE_USER = 'ROLE_USER';

   /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string|null
     */
    private $plainPassword;

    /**
     * @var string|null
     */
    private $validationToken;

    /**
     * @var DateTime|null
     */
    private $validatedAt;

    /**
     * @var string|null
     */
    private $resetToken;

    /**
     * @var DateTime|null
     */
    private $resetTokenExpiredAt;

    /**
     * @var Collection
     */
    private $tricks;

    /**
     * @var Collection
     */
    private $comments;

    /**
     * @var string
     */
    private $name;

    public function __construct()
    {
        $this->roles = [self::ROLE_USER];
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set salt.
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt.
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set roles.
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles.
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     * @return User
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Set validationToken.
     *
     * @param string|null $validationToken
     *
     * @return User
     */
    public function setValidationToken($validationToken = null)
    {
        $this->validationToken = $validationToken;

        return $this;
    }

    /**
     * Get validationToken.
     *
     * @return string|null
     */
    public function getValidationToken()
    {
        return $this->validationToken;
    }

    /**
     * Set validatedAt.
     *
     * @param DateTime|null $validatedAt
     *
     * @return User
     */
    public function setValidatedAt($validatedAt = null)
    {
        $this->validatedAt = $validatedAt;

        return $this;
    }

    /**
     * Get validatedAt.
     *
     * @return DateTime|null
     */
    public function getValidatedAt()
    {
        return $this->validatedAt;
    }

    /**
     * Set resetToken.
     *
     * @param string|null $resetToken
     *
     * @return User
     */
    public function setResetToken($resetToken = null)
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    /**
     * Get resetToken.
     *
     * @return string|null
     */
    public function getResetToken()
    {
        return $this->resetToken;
    }

    /**
     * Set resetTokenExpiredAt.
     *
     * @param DateTime|null $resetTokenExpiredAt
     *
     * @return User
     */
    public function setResetTokenExpiredAt($resetTokenExpiredAt = null)
    {
        $this->resetTokenExpiredAt = $resetTokenExpiredAt;

        return $this;
    }

    /**
     * Get resetTokenExpiredAt.
     *
     * @return DateTime|null
     */
    public function getResetTokenExpiredAt()
    {
        return $this->resetTokenExpiredAt;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
        //$this→password = null; //this breaks login
    }

    /**
     * Set userName.
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get username.
     * Used by auth
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }


    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * @param UserInterface $user
     * @return bool
     * @throws Exception
     */
    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        return true;
    }

    /**
     * @param Trick $trick
     * @return User
     */
    public function addTrick(Trick $trick) : User
    {
        $this->tricks[] = $trick;

        $trick->setUser($this);

        return $this;
    }

    /**
     * @param Trick $trick
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrick(Trick $trick)
    {
        return $this->tricks->removeElement($trick);
    }

    /**
     * @return Collection
     */
    public function getTricks()
    {
        return $this->tricks;
    }

    /**
     * @param Comment $comment
     * @return User
     */
    public function addComment(Comment $comment) : User
    {
        $this->comments[] = $comment;

        $comment->setUser($this);

        return $this;
    }

    /**
     * @param Comment $comment
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeComment(Comment $comment)
    {
        return $this->comments->removeElement($comment);
    }

    /**
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
}
