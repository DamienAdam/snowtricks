<?php

namespace App\Domain\User\Command;

use App\Domain\User\Doctrine\Entity\User;
use App\Domain\User\Manager\UserManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:user:create-user';

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * CreateUserCommand constructor.
     * @param UserManager $userManager
     * @param string|null $name
     */
    public function __construct(UserManager $userManager, string $name = null)
    {
        parent::__construct($name);
        $this->userManager = $userManager;
    }


    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Create a new User.')
            ->addOption('email', 'em', InputOption::VALUE_REQUIRED)
            ->addOption('name', 'un', InputOption::VALUE_REQUIRED)
            ->addOption('password', 'p', InputOption::VALUE_REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = new User();
        $user->setEmail($input->getOption('email'));
        $user->setName($input->getOption('name'));
        $user->setPlainPassword($input->getOption('password'));

        $this->userManager->createUser($user, false);
    }
}