<?php

namespace App\Domain\User\Mail;

use App\Domain\User\Doctrine\Entity\User;
use App\Infrastructure\Mailer\Mailjet\Form\Command\MailjetCommand;
use App\Infrastructure\Mailer\Mailjet\Form\Command\MailjetContactCommand;
use App\Infrastructure\Mailer\Mailjet\Form\Command\MailjetVariableCommand;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AccountValidationMail
{
    const MAILJET_TEMPLATE_ID = 2199311;

    /**
     * @var MailjetCommand
     */
    private $mailjetCommand;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(User $user, UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
        $this->mailjetCommand = new MailjetCommand();

        $from = new MailjetContactCommand();
        $from->email = 'damien.adam@heureetcontrole.fr';
        $from->name = 'Snowtricks Housekeeping';
        $this->mailjetCommand->from = $from;

        $dest = new MailjetContactCommand();
        $dest->email = $user->getEmail();
        $dest->name = $user->getUsername();
        $this->mailjetCommand->dest = $dest;

        $this->mailjetCommand->subject = 'Your signup validation link';

        $var1 = new MailjetVariableCommand();
        $var1->key = 'validationLink';
        $var1->value = $this->urlGenerator->generate('security_account_validator', [
                'validationToken' => $user->getValidationToken()
            ],UrlGeneratorInterface::ABSOLUTE_URL);

        $this->mailjetCommand->vars[] = $var1;

        $this->mailjetCommand->templateId = self::MAILJET_TEMPLATE_ID;
    }

    /**
     * @return MailjetCommand
     */
    public function getMailjetCommand(): MailjetCommand
    {
        return $this->mailjetCommand;
    }
}