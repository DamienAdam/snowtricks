<?php

namespace App\Domain\Comment\Manager;

use App\Domain\Comment\Doctrine\Entity\Comment;
use App\Domain\Comment\Doctrine\Repository\CommentRepository;
use App\Domain\Trick\Doctrine\Entity\Trick;
use App\Domain\User\Doctrine\Entity\User;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class CommentManager
{
    /**
     * @var CommentRepository
     */
    private $commentRepository;

    /**
     * @var int
     */
    private $commentsPerPage;

    public function __construct(CommentRepository $commentRepository, int $commentsPerPage)
    {
        $this->commentRepository = $commentRepository;
        $this->commentsPerPage = $commentsPerPage;
    }

    /**
     * @param Comment $comment
     * @param Trick $trick
     * @param User $user
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Comment $comment, Trick $trick, User $user)
    {
        $comment->setUser($user);
        $comment->setTrick($trick);
        $this->commentRepository->save($comment);
    }

    public function loadTrickComments(Trick $trick, int $page)
    {
        return $this->commentRepository->findPaginated($trick, $this->commentsPerPage, $page);
    }
}