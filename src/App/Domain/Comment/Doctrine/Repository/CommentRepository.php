<?php

namespace App\Domain\Comment\Doctrine\Repository;

use App\Common\Doctrine\Repository\BaseRepository;
use App\Domain\Trick\Doctrine\Entity\Trick;

class CommentRepository extends BaseRepository
{
    public function findPaginated(Trick $trick, int $maxResults, int $page)
    {
        $firstResult = ($page - 1) * $maxResults;
        $queryBuilder = $this->createQueryBuilder('c');
        $queryBuilder->setFirstResult($firstResult);
        $queryBuilder->setMaxResults($maxResults);

        $queryBuilder
            ->andWhere('c.trick = :trick')
            ->setParameter('trick', $trick)
            ->orderBy('c.createdAt', 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }
}