function addTagFormDeleteLink($tagFormLi) {

    let $removeFormButton = $(
        "<div class=\"removeCollectionItem align-self-end\">" +
        "<span class=\"bx bx-x\"></span>" +
        "</div>"
    );

    $tagFormLi.find(".deletable").append($removeFormButton);

    $removeFormButton.on("click", function(e) {
        // remove the li for the tag form
        $tagFormLi.remove();
    });
}

$(document).ready(function () {

    // Get the ul that holds the collection of tags
    let $collectionHolder = $("#trick_trickVideos");

    // add a delete link to all of the existing tag form li elements
    $collectionHolder.find(".trickVideo").each(function() {
        addTagFormDeleteLink($(this));
    });

    $(".add-collection-btn").click(function (e) {
        let list = $($(this).attr("data-list-selector"));
        let counter = list.data("widget-counter") || list.children().length;

        let newWidget = list.attr("data-prototype");
        newWidget = newWidget.replace(/__name__/g, counter);


        counter++;
        list.data('widget-counter', counter);

        let newElem = $(list.attr("data-widget-tags")).html(newWidget);
        newElem.appendTo(list);

        addTagFormDeleteLink(newElem);
    });

});


